package com.example.olio_ohjelmointi_ht;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.olio_ohjelmointi_ht.io.JSONParser;
import com.example.olio_ohjelmointi_ht.recyclingPoint.Location;
import com.example.olio_ohjelmointi_ht.recyclingPoint.RecycleItem;
import com.example.olio_ohjelmointi_ht.api.RecyclePointAPIManager;
import com.example.olio_ohjelmointi_ht.recyclingPoint.RecycleViewAdapter;
import com.example.olio_ohjelmointi_ht.recyclingPoint.RecyclingPoint;

import org.json.JSONObject;

import java.util.ArrayList;

public class Fragment3 extends Fragment implements View.OnClickListener {

    View view;
    Button locationBtn;
    Location curLocation;
    EditText municipality;
    TextView textView;
    ArrayList<RecycleItem> recycleList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter recycleAdapter;
    private RecyclerView.LayoutManager layoutManager;
    int recycleOptions, image;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.f_fragment3, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = this.view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        locationBtn = this.view.findViewById(R.id.button3);
        locationBtn.setOnClickListener(this);

        textView = this.view.findViewById(R.id.textView10);
        if (curLocation == null) {
            textView.setText("Set location first");
        } else {
            textView.setText("Municipality: "+curLocation.getMunicipality());
        }
    }

    @Override
    public void onClick(View v) {
        if (v == locationBtn) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

            View dialogView = getLayoutInflater().inflate(R.layout.f_location, null);
            dialog.setView(dialogView);

            municipality = dialogView.findViewById(R.id.editTextTextPostalAddress3);

            recyclerView = this.view.findViewById(R.id.recyclerView);
            recyclerView.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(getActivity().getBaseContext());
            recycleAdapter = new RecycleViewAdapter(recycleList, getActivity().getApplicationContext());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(recycleAdapter);

            dialog.setPositiveButton("Locate", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (municipality.getText().toString().isEmpty()) {
                        Toast.makeText(getActivity(),"Fill all fields", Toast.LENGTH_SHORT).show();
                    } else {
                        Location loc = new Location(municipality.getText().toString());
                        setCurLocation(loc);
                        textView.setText("Municipality: "+curLocation.getMunicipality());
                        RecyclePointAPIManager locM = new RecyclePointAPIManager();
                        String url = locM.answersToUrlEnd(curLocation.getMunicipality());
                        JSONObject locations = locM.getCollectionPoints(url);
                        JSONParser jsonParser = new JSONParser();
                        ArrayList<RecyclingPoint> tpLocArray = jsonParser.jsonParser(locations);
                        recycleList.clear();
                        for (RecyclingPoint tpl : tpLocArray) {
                            recycleOptions = tpl.getMats().size();
                            image = getImage(recycleOptions);
                            RecycleItem ri = new RecycleItem(R.drawable.ic_question_mark_line, tpl.getName(), tpl.getAddress(), tpl.getMats(), image);
                            recycleList.add(ri);
                        }
                        recycleAdapter = new RecycleViewAdapter(recycleList, getActivity().getApplicationContext());
                        recyclerView.setAdapter(recycleAdapter);

                    }
                }
            });

            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            dialog.show();
        }

    }

    /*
    Chooses image based on the amount of recycle facilities are the location
     */
    private int getImage(int recycleOptions) {
        int result;
        if (recycleOptions <= 1) {
            result = R.drawable.ic_angry_face;
        } else if (recycleOptions > 1 && recycleOptions <= 3) {
            result = R.drawable.ic_crying_line;
        } else if (recycleOptions > 3 && recycleOptions <= 6) {
            result = R.drawable.ic_confused_line;
        } else {
            result = R.drawable.ic_laughing_line;
        }
        return result;
    }

    public void setCurLocation(Location curLocation) {
        this.curLocation = curLocation;
    }



}
