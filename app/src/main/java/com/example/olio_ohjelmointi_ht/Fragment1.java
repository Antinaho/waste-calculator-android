package com.example.olio_ohjelmointi_ht;

import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.olio_ohjelmointi_ht.api.WasteAPIManager;
import com.example.olio_ohjelmointi_ht.io.FileManager;

import java.util.ArrayList;

public class Fragment1 extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    Spinner spinner1, spinner2, spinner3, spinner4, spinner5, spinner6, spinner7, spinner8, spinner9;
    View view;
    Button calcButton;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        view = inflater.inflate(R.layout.f_fragment1, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinner1 = this.view.findViewById(R.id.spinner10);
        spinner2 = this.view.findViewById(R.id.spinner11);
        spinner3 = this.view.findViewById(R.id.spinner12);
        spinner4 = this.view.findViewById(R.id.spinner13);
        spinner5 = this.view.findViewById(R.id.spinner14);
        spinner6 = this.view.findViewById(R.id.spinner15);
        spinner7 = this.view.findViewById(R.id.spinner16);
        spinner8 = this.view.findViewById(R.id.spinner17);
        spinner9 = this.view.findViewById(R.id.spinner18);
        calcButton = this.view.findViewById(R.id.button);

        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(getActivity().getBaseContext(), R.array.waste_answers_1, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> spinnerAdapter2 = ArrayAdapter.createFromResource(getActivity().getBaseContext(), R.array.waste_answers_2, android.R.layout.simple_spinner_item);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner1.setAdapter(spinnerAdapter);
        spinner2.setAdapter(spinnerAdapter);
        spinner3.setAdapter(spinnerAdapter);
        spinner4.setAdapter(spinnerAdapter);
        spinner5.setAdapter(spinnerAdapter);
        spinner6.setAdapter(spinnerAdapter);
        spinner7.setAdapter(spinnerAdapter);
        spinner8.setAdapter(spinnerAdapter);
        spinner9.setAdapter(spinnerAdapter2);

        spinner1.setOnItemSelectedListener(this);
        calcButton.setOnClickListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        if (v == calcButton) {
            ArrayList<String> answers = new ArrayList<>();
            answers.add(spinner1.getSelectedItem().toString());
            answers.add(spinner2.getSelectedItem().toString());
            answers.add(spinner3.getSelectedItem().toString());
            answers.add(spinner4.getSelectedItem().toString());
            answers.add(spinner5.getSelectedItem().toString());
            answers.add(spinner6.getSelectedItem().toString());
            answers.add(spinner7.getSelectedItem().toString());
            answers.add(spinner8.getSelectedItem().toString());
            answers.add(spinner9.getSelectedItem().toString());
            String co2 = calcCO2(answers);
            System.out.println(co2);
            if (!co2.equals("")) {
                FileManager fm = new FileManager();
                fm.saveToFile(getActivity().getBaseContext(), co2);
                Toast.makeText(getActivity().getBaseContext(), "Results: "+co2+" kg CO2-eq/year", Toast.LENGTH_LONG).show();
            }


        }
    }

    /*
    Calculate the users CO2 emissions
     */
    private String calcCO2(ArrayList<String> answers) {
        String url, amount;
        WasteAPIManager apiM = new WasteAPIManager();
        url = apiM.answersToUrlEnd(answers);
        amount = apiM.getCO2Amount(url);
        return amount != null ? amount : "";
    }

}
