package com.example.olio_ohjelmointi_ht.io;

import android.content.Context;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;

public class CSVParser {

    /*
    Parses file and returns entries between start date and end date
     */
    public ArrayList<String> parseCSV(String startDate, String endDate, Context context) {
        FileManager fm = new FileManager();
        String fileText = fm.readFile(context);
        String[] separated = fileText.split("\n");
        String[] data = Arrays.copyOfRange(separated, 1, separated.length);
        Date start = Date.valueOf(convertDateFormat(startDate));
        Date end = Date.valueOf(convertDateFormat(endDate));
        ArrayList<String> desiredDates = new ArrayList<>();

        for (String s : data) {
            String[] dateAmount = s.split(",");
            Date date = Date.valueOf(convertDateFormat(dateAmount[0]));
            if ((date.before(end) && date.after(start)) || date.equals(start) || date.equals(end)) {
                desiredDates.add(s);
            }
        }
        return desiredDates;
    }

    /*
    Converts dates to format which can be used by Date.Valueof()
     */
    private String convertDateFormat(String dateString) {
        String mm = dateString.substring(3,5);
        String dd = dateString.substring(0,2);
        String yy = dateString.substring(6);
        return yy+"-"+mm+"-"+dd;
    }


}
