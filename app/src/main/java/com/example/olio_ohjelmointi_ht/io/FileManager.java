package com.example.olio_ohjelmointi_ht.io;

import android.content.Context;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class FileManager {

    private static final String FILE_CO2_NAME = "wasteHistory.csv";

    /*
    Saves given data to file with current date
     */
    public void saveToFile(Context context, String data) {
        DateFormat formatter = DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE);
        FileOutputStream fo = null;
        Date date = new Date();
        String text = (formatter.format(date)+","+data+"\n");
        String firstLine = "Date,Waste estimate\n";
        String[] files = context.fileList();

        try {
            if (files.length > 0) {
                for (String fileName : files) {
                    if (fileName.equals(FILE_CO2_NAME)) {
                        System.out.println("Appending to file");
                        fo = context.openFileOutput(FILE_CO2_NAME, Context.MODE_APPEND);
                        fo.write(text.getBytes());
                        break;
                    }
                }
                if (fo == null) {
                    fo = context.openFileOutput(FILE_CO2_NAME, Context.MODE_PRIVATE);
                    fo.write(firstLine.getBytes());
                    fo.write(text.getBytes());
                }
            } else {
                fo = context.openFileOutput(FILE_CO2_NAME, Context.MODE_PRIVATE);
                fo.write(firstLine.getBytes());
                fo.write(text.getBytes());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fo != null) {
                try {
                    fo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*
    Returns the contents of the waste history file as String
     */
    public String readFile(Context context) {
        FileInputStream fi = null;

        try {
            fi = context.openFileInput(FILE_CO2_NAME);
            InputStreamReader isr = new InputStreamReader(fi);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;

            while ((text = br.readLine()) != null) {
                sb.append(text+"\n");
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fi != null) {
                try {
                    fi.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
