package com.example.olio_ohjelmointi_ht.recyclingPoint;

import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class RecycleItem {
    private int mImageResource, mRating;
    private String rName, rAddress;
    private ArrayList<String> matArray;

    public RecycleItem(int imageResource, String name, String address, ArrayList<String> matArray, int mRating) {
        mImageResource = imageResource;
        rName = name;
        rAddress = address;
        this.matArray = matArray;
        this.mRating = mRating;
    }

    public int getmImageResource() {
        return mImageResource;
    }

    public String getrName() {
        return rName;
    }

    public String getrAddress() {
        return rAddress;
    }

    public ArrayList<String> getMatArray() {
        return matArray;
    }

    public int getmRating() {
        return mRating;
    }
}
