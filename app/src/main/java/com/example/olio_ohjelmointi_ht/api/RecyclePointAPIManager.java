package com.example.olio_ohjelmointi_ht.api;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class RecyclePointAPIManager {

    private static final String API_KEY = "38e9a54a4b59493f3466e6bd85c137b0f5910633";
    private static final String URL = "https://api.kierratys.info/collectionspots/?api_key="+API_KEY;

    private static final int TIMEOUT = 8000;

    public String answersToUrlEnd(String municipality) {
        return "&municipality="+municipality;
    }

    /*
    Gets recycling locations with given url
    */
    public JSONObject getCollectionPoints(String queryUrl) {
        StringBuilder content = new StringBuilder();
        try {
            java.net.URL url = new URL(URL + queryUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            con.setConnectTimeout(TIMEOUT);
            con.setReadTimeout(TIMEOUT);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();
            JSONObject response = new JSONObject(content.toString());
            return response;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }



}
