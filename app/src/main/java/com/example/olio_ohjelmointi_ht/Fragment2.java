package com.example.olio_ohjelmointi_ht;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.olio_ohjelmointi_ht.io.CSVParser;
import com.example.olio_ohjelmointi_ht.io.FileManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class Fragment2 extends Fragment implements View.OnClickListener {

    View view;
    Button startDateBtn, endDateBtn, loadBtn;
    String startDate, endDate;
    LinearLayout linearLayout;

    ArrayList<String> userSelectedDates;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.f_fragment2, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        startDateBtn = this.view.findViewById(R.id.button2);
        startDateBtn.setOnClickListener(this);

        endDateBtn = this.view.findViewById(R.id.button4);
        endDateBtn.setOnClickListener(this);

        loadBtn = this.view.findViewById(R.id.button5);
        loadBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == startDateBtn || v == endDateBtn) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    getActivity(),
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            String d = String.valueOf(dayOfMonth), m = String.valueOf(month+1);
                            if (String.valueOf(dayOfMonth).length() == 1) {
                                d = "0"+dayOfMonth;
                            }
                            if (String.valueOf(month).length() == 1) {
                                m = "0"+m;
                            }
                            if (v == startDateBtn) {
                                startDate = d+"."+m+"."+year;
                                startDateBtn.setText(startDate);
                            } else if (v == endDateBtn) {
                                endDate = d+"."+m+"."+year;
                                endDateBtn.setText(endDate);
                            }
                        }
                    },
                    Calendar.getInstance().get(Calendar.YEAR),
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
            );
            datePickerDialog.show();
        } else if (v == loadBtn) {
            linearLayout = this.view.findViewById(R.id.LinearLayout);
            linearLayout.removeAllViews();
            CSVParser csvParser = new CSVParser();
            userSelectedDates = csvParser.parseCSV(startDate, endDate, getActivity().getBaseContext());

            System.out.println(userSelectedDates);
            for (String entry : userSelectedDates) {
                createNewTextView(entry);
            }
        }
    }

    private void createNewTextView(String entry) {
        TextView textView = new TextView(getActivity());

        textView.setPadding(50,4,0,0);
        textView.setTextSize(14);
        textView.setText(entry.split(",")[0]+"\t\t"+entry.split(",")[1]);
        textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        linearLayout.addView(textView);
    }
}
