package com.example.olio_ohjelmointi_ht.recyclingPoint;

public class Location {

    String municipality;

    public Location(String municipality) {
        this.municipality = municipality;
    }

    public String getMunicipality() {
        return municipality;
    }

}
