package com.example.olio_ohjelmointi_ht.recyclingPoint;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.olio_ohjelmointi_ht.R;

import java.util.ArrayList;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.RecycleViewHolder> {

    private ArrayList<RecycleItem> recycleList;
    private Context ctx;

    public RecycleViewAdapter(ArrayList<RecycleItem> recycleItemArrayList, Context ctx) {
        recycleList = recycleItemArrayList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(ctx).inflate(R.layout.recycle_item, parent, false);
        RecycleViewHolder rvh = new RecycleViewHolder(v);
        return rvh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecycleViewHolder holder, int position) {
        RecycleItem currentItem = recycleList.get(position);
        ArrayList<String> mats = currentItem.getMatArray();

        holder.imageView2.setImageResource(currentItem.getmRating());
        holder.imageView.setImageResource(currentItem.getmImageResource());

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int matsN = mats.size();
                String toastMsg = "Available recycling points:\n"+String.join("\n", mats);
                if (matsN <= 4) {
                    Toast.makeText(ctx,toastMsg,Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ctx,toastMsg,Toast.LENGTH_LONG).show();
                }

            }
        });
        holder.textView1.setText(currentItem.getrName());
        holder.textView2.setText(currentItem.getrAddress());
    }

    @Override
    public int getItemCount() {
        return recycleList.size();
    }

    public static class RecycleViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView, imageView2;
        public TextView textView1, textView2;

        public RecycleViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textView1 = itemView.findViewById(R.id.textLine2);
            textView2 = itemView.findViewById(R.id.textLine1);
            imageView2 = itemView.findViewById(R.id.imageView2);

        }
    }


}
