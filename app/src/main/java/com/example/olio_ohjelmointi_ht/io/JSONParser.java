package com.example.olio_ohjelmointi_ht.io;

import com.example.olio_ohjelmointi_ht.recyclingPoint.RecyclingPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JSONParser {

    /*
    Get array of recycling points from JSONObject
     */
    public ArrayList<RecyclingPoint> jsonParser(JSONObject obj) {
        String name, address;
        ArrayList<RecyclingPoint> recyclingPoints = new ArrayList<>();
        try {
            JSONArray resultArray = obj.getJSONArray("results");
            for (int i = 0; i < resultArray.length(); i++) {
                ArrayList<String> recycleMaterial = new ArrayList<>();
                JSONObject details = resultArray.getJSONObject(i);
                name = details.getString("name");
                address = details.getString("address");
                JSONArray matArray = details.getJSONArray("materials");
                for (int j = 0; j < matArray.length(); j++) {
                    JSONObject materials = matArray.getJSONObject(j);
                    String m = materials.getString("name");
                    recycleMaterial.add(m);
                }
                RecyclingPoint rPoint = new RecyclingPoint(name, address, recycleMaterial);
                recyclingPoints.add(rPoint);
            }
            return recyclingPoints;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
