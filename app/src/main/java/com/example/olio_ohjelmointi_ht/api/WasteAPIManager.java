package com.example.olio_ohjelmointi_ht.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WasteAPIManager {

    private static final String URL_START = "https://ilmastodieetti.ymparisto.fi/ilmastodieetti/calculatorapi/v1/WasteCalculator?";
    private static final String[] QUERY_QUESTIONS = {
            "bioWaste",
            "carton",
            "electronic",
            "glass",
            "hazardous",
            "metal",
            "paper",
            "plastic",
            "amountEstimate"
    };
    private static final int TIMEOUT = 8000;

    /*
    Return full string for http request based on user's answers;
     */
    public String answersToUrlEnd(ArrayList<String> answers) {
        StringBuilder res = new StringBuilder();
        HashMap<String, String> pair = new HashMap<>();

        for (int i = 0; i < QUERY_QUESTIONS.length; i++) {
            pair.put(QUERY_QUESTIONS[i], answers.get(i));
        }
        for (Map.Entry<String, String> entry : pair.entrySet()) {
            String s = "query."+entry.getKey()+"="+entry.getValue()+"&";
            res.append(s);
        }
        String result = res.toString();

        return result.substring(0, result.length()-1);
    }

    /*
    Get the user's estimated CO2 emissions from external API
     */
    public String getCO2Amount(String queryUrl) {
        StringBuilder content = new StringBuilder();
        try {
            URL url = new URL(URL_START + queryUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            con.setConnectTimeout(TIMEOUT);
            con.setReadTimeout(TIMEOUT);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();
            return content.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
