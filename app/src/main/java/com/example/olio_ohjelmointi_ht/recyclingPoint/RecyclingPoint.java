package com.example.olio_ohjelmointi_ht.recyclingPoint;

import java.util.ArrayList;

public class RecyclingPoint {

    private String name, address;
    private ArrayList<String> mats;

    public RecyclingPoint(String name, String address, ArrayList<String> mats) {
        this.name = name;
        this.address = address;
        this.mats = mats;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public ArrayList<String> getMats() {
        return mats;
    }
}
